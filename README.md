# Tanss Tab Renamer
Eine Erweiterung für Firefox, welche im Ticketsystem Tanss im Tabtitel die Ticketnummer, bzw. die Leistungsnummer anzeigt.

## Funktionen
- Anzeigen der Ticketnummer im Titel
- Anzeigen der Leistungsnummer im Titel

## Abhänigkeiten
- Firefox Desktop

## Installation
Installation via [addons.mozilla.org](https://addons.mozilla.org/de/firefox/addon/tanss-title-changer/)

## Lizenz und Verwendung
Diese Projekt wird unter GPLv3 bereitgestellt.
